﻿using UnityEngine;
using System.Collections;
using CharacterEngine.Primitives;

public class Test : MonoBehaviour {

    public Transform a;
    public Transform b;
    public BoxCollider2D c;
    public BoxCollider2D c1;
    public BoxCollider2D c2;

    private CECollider cec;
    private CECollider cec1;
    private CECollider cec2;
    // Use this for initialization
    void Start () {
        cec = new CECollider(c);
        cec1 = new CECollider(c1);
        cec2 = new CECollider(c2);

    }

    // Update is called once per frame
    void Update () {
        Vector2 axis = (a.position - b.position).normalized;
        CEProjection p = new CEProjection(cec, axis);
        CEProjection p1 = new CEProjection(cec1, axis);
        CEProjection p2 = new CEProjection(cec2, axis);

        Debug.DrawLine(axis * p.min, axis * p.max, Color.red);
        Debug.DrawLine(axis * p1.min, axis * p1.max, Color.blue);
        Debug.DrawLine(axis * p2.min, axis * p2.max, Color.yellow);
    }
}
