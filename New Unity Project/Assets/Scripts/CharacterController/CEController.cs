﻿using UnityEngine;
using System.Collections;
using CharacterEngine.Primitives;

namespace CharacterEngine.Controller
{
    public class CEController : MonoBehaviour
    {

        #region Public Custom Fields

        [Range(0.5f, 3.0f)]
        public float skinWidth = 1.0f;

        [Range(0.0f, 5.0f)]
        public float weight = 1.0f;

        #endregion

        protected Vector2 moveVelocity;

        private CEPrimitives primitives;

        /*Summary:
            Initializer region contains both native unity initalizers of monobehaviour, Awake is called ONLY ONCE IN 
            LIFE TIME, and is called when the object is being created, before Start. Start in the other hand, is called
            when the object is enable and ready. 
        */

        void Awake ()
        {
            primitives = new CEPrimitives(this);
        }

        /*Summary:
            In Updates, we have both functions native from unity Monobehaviour: Update & FixedUpdate.
            The difference of this both is that one (Update) excecute every frame, and the other one, is executed
            by a fixed time (Fixed Update) which is by default 0.02s = 20 ms, that means "Update" depends on the framerate
            while "Fixed Update" dont. 
        */

        void FixedUpdate ()
        {
            MoveStep();
            primitives.Step();
        }

        void MoveStep ()
        {
            primitives.AddVelocity(moveVelocity);
        }

        public void Move (Vector2 velocity)
        {
            moveVelocity = velocity;
        }
    }
}
