﻿using UnityEngine;
using System.Collections;
using CharacterEngine.Controller;

public class Player : MonoBehaviour {

    protected CEController controller;
    protected bool grounding;

    void Awake ()
    {
        controller = GetComponent<CEController>();
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0.3f)
        {
            float value = Mathf.Sign(Input.GetAxis("Horizontal"));
            controller.Move(Vector3.right * value);
        }
        /*
        if (Input.GetButtonDown("A") && grounding)
        {
            //controller.AddForce(forces[1], Vector3.up);
        }
        */
    }

    void OnLanded ()
    {
        /*
        controller.DestroyForce(forces[1]);
        velocity.y = 0.0f;
        */
    }

}
